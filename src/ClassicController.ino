// ATtiny85 @ 16 MHz (internal PLL; BOD disabled)
// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//                            +-\/-+
//                  RES/PB5  1|    |8  Vcc
//                      PB3  2|    |7  PB2/SCLK
//                      PB4  3|    |6  PB1/MISO
//                      GND  4|    |5  PB0/MOSI
//                            +----+
//

#include <Arduino.h>
#include "wm_crypto.h"
#include "usitwislave.h"
#include <EEPROM.h>

// Identification sequence for Classic Controller
const byte idbuf[] = { 0x00, 0x00, 0xa4, 0x20, 0x01, 0x01 };

// Classic Controller calibration data
byte calbuf[] = { 0xFC, 0x04, 0x7E, 0xFC, 0x04, 0x7E, 0xFC, 0x04, 0x7E,
                  0xFC, 0x04, 0x7E, 0x00, 0x00, 0x00, 0x00 }; // Checksum will be calculated later...

// Global array for storing and accessing register values written by the Wiimote.
// 256 registers corresponding to the extension, starting at 04a40000.
byte wiimote_registers[0x100];

static byte state = 0;
static byte crypt_setup_done = 0;

// Classic Controller Analog Sticks
// They are initialized with center values from the calibration buffer.
byte lx = calbuf[2]>>2;
byte ly = calbuf[5]>>2;
byte rx = calbuf[8]>>3;
byte ry = calbuf[11]>>3;

// Include controller type header
#include "nes.h"
//#include "snes.h"
//#include "n64.h"
//#include "genesis.h"
//#include "neogeo.h"

// Start Wiimote <-> Extension communication encryption, if requested by the application.
static void setup_encryption()
{
  int i;

  for (i = 0x40; i <= 0x49; i++) wm_rand[9 - (i - 0x40)] = wiimote_registers[i];
  for (i = 0x4A; i <= 0x4F; i++) wm_key[5 - (i - 0x4A)] = wiimote_registers[i];

  wm_gentabs();
  crypt_setup_done = 1;
}

static void twi_data_callback(volatile uint8_t input_buffer_length, const volatile uint8_t *input_buffer,
                              volatile uint8_t *output_buffer_length, volatile uint8_t *output_buffer)
{
  byte crypt_keys_received = 0;
  static byte last_state = 0xFF;
  static byte offset = 0;
  uint8_t addr;

  if (input_buffer_length == 0) return; // NOTHING TO DO HERE!

  if (input_buffer_length == 1)
  {
    state = input_buffer[0];
  }
  else if (input_buffer_length > 1)
  {
    byte addr = input_buffer[0];
    byte curr = addr;

    for (int i = 1; i < input_buffer_length; i++)
    {
      byte d = input_buffer[i];

      // Wii is trying to disable encryption
      if(addr == 0xF0 && (d == 0x55 || d == 0xAA)) crypt_setup_done = 0;

      if (crypt_setup_done)
      {
        // Decrypt
        wiimote_registers[curr++] = (d ^ wm_sb[curr % 8]) + wm_ft[curr % 8];
      }
      else
      {
        wiimote_registers[curr++] = d;
      }

      // Check if last crypt key setup byte was received
      if (curr == 0x50) crypt_keys_received = 1;
    }
  }

  // Setup encryption
  if (crypt_keys_received) setup_encryption();

  // --- Handle request -----------------------
  switch (state)
  {
    case 0x00:
      // We need to send controller data, so query the controller!
      controller_query();
      // Pack the controller data into Classic Controller format
      pack_classic_data(wiimote_registers);
      // Send it!
      addr = 0x00;
      *output_buffer_length = 6;
      memcpy((void*)output_buffer, &wiimote_registers, 6);
    break;

    case 0xFA:
      addr = state;
      *output_buffer_length = 6;
      memcpy((void*)output_buffer, &wiimote_registers[state], 6);
    break;

    default:
      if(last_state == state)
      {
        offset += 8;
      }
      else
      {
        last_state = state;
        offset = 0;
      }

      addr = state + offset;
      *output_buffer_length = 8;
      memcpy((void*)output_buffer, &wiimote_registers[state + offset], 8);
    break;
  }

  // Encrypt bytes to be sent back, if necessary
  if (wiimote_registers[0xF0] == 0xAA && crypt_setup_done)
  {
    for (int i = 0; i < *output_buffer_length; i++)
      output_buffer[i] = (output_buffer[i] - wm_ft[(addr + i) % 8]) ^ wm_sb[(addr + i) % 8];
  }
}

void setup()
{
  controller_init();

  // Initialize the wiimote registers
  memset(wiimote_registers, 0xFF, 0x100);

  // Set extension id on registers
  for (int i = 0xFA; i <= 0xFF; i++) wiimote_registers[i] = idbuf[i - 0xFA];

  // Fix calibration data checksum, just in case...
  byte calchecksum = 0;
  for (int i = 0; i < 14; i++) calchecksum += calbuf[i];

  calbuf[14] = calchecksum + 0x55;
  calbuf[15] = calchecksum + 0xAA;

  // Set calibration data on registers
  for (int i = 0x20; i <= 0x2F; i++)
  {
    wiimote_registers[i] = calbuf[i - 0x20]; // 0x20
    wiimote_registers[i + 0x10] = calbuf[i - 0x20]; // 0x30
  }

  // Initialize Classic Controller data with calibration data
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  wiimote_registers[3] = 0x00;
  wiimote_registers[4] = 0xFF;
  wiimote_registers[5] = 0xFF;

  // Encryption disabled by default
  wiimote_registers[0xF0] = 0x55;
  wiimote_registers[0xFB] = 0x00;

  // Join I2C bus
  usi_twi_slave(0x52, 1, twi_data_callback, NULL);
}

void loop() { }
