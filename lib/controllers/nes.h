// -----------------------------------------------------------------
// NES Controller Pinout
// -----------------------------------------------------------------
//          .-
//    GND - |O \
//    CLK - |O O\ - +5V
//  LATCH - |O O| - N/C
//   DATA - |O O| - N/C
//          '---'

// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//         !! CLOCK AND DATA SWAPPED FROM SNES PINTOUT !!
//                            +-\/-+
//                  RES/PB5  1|    |8  Vcc
//   (yellow) NES Clk - PB3  2|    |7  PB2/SCLK - Wiimote SCL (blue)
//     (red) NES Data - PB4  3|    |6  PB1/MISO - NES Latch (orange)
//                      GND  4|    |5  PB0/MOSI - Wiimote SDA (green)
//                            +----+

// NES controller modes
enum {
  DPAD_ANALOG_MODE,  // Maps d-pad to d-pad AND analog. Good for Wii
  DPAD_MODE,         // Maps d-pad to only d-pad. Wii U doesn't like analog too!
};
byte NES_mode = DPAD_ANALOG_MODE;
byte NES_last_mode;

byte NES_status;

// NES controller pin defines
#define NES_LATCH_DDR   DDRB
#define NES_LATCH_PORT  PORTB
#define NES_LATCH_BIT   0x02

#define NES_CLOCK_DDR   DDRB
#define NES_CLOCK_PORT  PORTB
#define NES_CLOCK_BIT   0x10

#define NES_DATA_PORT   PORTB
#define NES_DATA_DDR    DDRB
#define NES_DATA_PIN    PINB
#define NES_DATA_BIT    0x08

void controller_init()
{
  // Set SNES controller pin directions
  // Clock and latch as output
  // Data as input
  NES_LATCH_DDR |= NES_LATCH_BIT;
  NES_CLOCK_DDR |= NES_CLOCK_BIT;
  NES_DATA_DDR &= ~(NES_DATA_BIT);

  // Enable pullup on data.
  // This should prevent random toggling
  NES_DATA_PORT |= NES_DATA_BIT;

  // Clock is normally high
  NES_CLOCK_PORT |= NES_CLOCK_BIT;

  // Latch is active-high
  NES_LATCH_PORT &= ~(NES_LATCH_BIT);

  // Set last used controller mode
  NES_mode = EEPROM.read(0);
  if (NES_mode > 2) NES_mode = DPAD_MODE;
  NES_last_mode = NES_mode;
}

void controller_query()
{
  /*
      Clock Cycle     Button Reported
      ===========     ===============
      1               A
      2               B
      3               Select
      4               Start
      5               Up on D-pad
      6               Down on D-pad
      7               Left on D-pad
      8               Right on D-pad
  */
  NES_status = 0;

  // Pulse Latch to store controller state
  NES_LATCH_PORT |= NES_LATCH_BIT;

  // Delay ~6us
  asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\n");

  NES_LATCH_PORT &= ~(NES_LATCH_BIT);

  // Shift out controller state bits (8 bits in total)
  for (int i = 0; i < 8; i++)
  {
    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Clock the next bit out
    NES_CLOCK_PORT &= ~(NES_CLOCK_BIT);

    NES_status >>= 1;

    // Read shifted bit (buttons are active-low)
    if (!(NES_DATA_PIN & NES_DATA_BIT)) NES_status |= 0x80;

    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Drop the clock to prepare for next bit
    NES_CLOCK_PORT |= NES_CLOCK_BIT;
  }
}

void pack_classic_data(byte *wiimote_registers)
{
  byte bdl     = (NES_status & 0x40  ? 1:0); // D-Pad Left state
  byte bdr     = (NES_status & 0x80  ? 1:0); // D-Pad Right state
  byte bdu     = (NES_status & 0x10  ? 1:0); // D-Pad Up state
  byte bdd     = (NES_status & 0x20  ? 1:0); // D-Pad Down state
  byte ba      = (NES_status & 0x01  ? 1:0); // A button state
  byte bb      = (NES_status & 0x02  ? 1:0); // B button state
  byte bstart  = (NES_status & 0x08  ? 1:0); // Start button state
  byte bselect = (NES_status & 0x04  ? 1:0); // Select button state
  byte bh      = 0;                          // Home button

  // Select + A|B to switch between controller modes
  // Selects mode then writes it to EEPROM so we don't forget it when we lose power
  if (bselect && ba)
  {
    NES_mode = DPAD_ANALOG_MODE;
    bselect = 0;
    ba = 0;
  }
  else if (bselect && bb)
  {
    NES_mode = DPAD_MODE;
    bselect = 0;
    bb = 0;
  }
  else if (bstart && bselect)
  {
    bh = 1;
    bstart = 0;
    bselect = 0;
  }

  // We only really care to write the new controller mode in EEPROM if it's changed
  // This will stop unnecessary repeated writes to the EEPROM
  if (NES_mode != NES_last_mode)
  {
    EEPROM.write(0, NES_mode);
    NES_last_mode = NES_mode;
  }

  // Map D-pad to analog stick for Wii menu control
  // Wii U doesn't like this! That's why we have DPAD_MODE
  if (NES_mode == DPAD_ANALOG_MODE)
  {
    if (bdu) ly = 44;
    else if (bdd) ly = 20;
    else ly = calbuf[5]>>2;

    if (bdr) lx = 44;
    else if (bdl) lx = 20;
    else lx = calbuf[2]>>2;
  }
  else
  {
    ly = calbuf[5]>>2;
    lx = calbuf[2]>>2;
  }

  // RX<4:3>, LX<5:0>
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  // RX<2:1>, LY<5:0>
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  // RX<0>, LT<4:3>, RY<4:0>
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  // LT<2:0>, RT<4:0>
  wiimote_registers[3] = 0x00;

  // BDR, BDD, BLT, B-, BH, B+, BRT, 1
  wiimote_registers[4] = (bdr      << 7) |
                         (bdd      << 6) |
                      // (bl       << 5) |
                         (bselect  << 4) |
                         (bh       << 3) |
                         (bstart   << 2);
                      // (br       << 1);

  // BZL, BB, BY, BA, BX, BZR, BDL, BDU
  wiimote_registers[5] =
                      // (bzl << 7) |   BZL not used
                         (bb  << 6) |
                      // (by  << 5) |
                         (ba  << 4) |
                      // (bx  << 3) |
                      // (bzr << 2) |   BZR not used
                         (bdl << 1) |
                         bdu;

  // Invert registers 4 and 5 since all buttons are active-low
  wiimote_registers[4] = ~wiimote_registers[4];
  wiimote_registers[5] = ~wiimote_registers[5];
}
