// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//                           +-\/-+
//                 RES/PB5  1|    |8  Vcc
//  (white) N64 Data - PB3  2|    |7  PB2/SCLK - Wiimote SCL (blue)
//                     PB4  3|    |6  PB1/MISO
//                     GND  4|    |5  PB0/MOSI - Wiimote SDA (green)
//                           +----+
// -----------------------------------------------------------------

#define N64_PIN   3
#define N64_HIGH  DDRB &= ~0x08
#define N64_LOW   DDRB |= 0x08
#define N64_QUERY (PINB & 0x08)

byte N64_data[4];

void controller_init()
{
  // Set N64 data pin mode first!
  // Makes sure we don't accidentally send 5V to the controller
  digitalWrite(N64_PIN, LOW);
  pinMode(N64_PIN, INPUT);
}

void controller_query()
{
  byte timeout, bitcount;

  // Make sure interrupts are disabled during this, since it's time-sensitive
  // Should already be disabled by twi_callback though

  // Send out poll command to N64 controller (0b000000011)
  // -----------------------------------------------------------------
  asm volatile ("ldi %[bitcount], 7\n"       // bitcount = 7 (7 N64 zero bits)
                "L_dl0%=:" "\n"              // JP0
                "sbi %[port], 3\n"           // N64 data low
                "nop\nnop\nnop\nnop\nnop\n"  // for 3us
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\n"
                "cbi %[port], 3\n"           // N64 data high
                "nop\nnop\nnop\nnop\nnop\n"  // for 1us
                "nop\nnop\nnop\nnop\nnop\n"
                "dec %[bitcount]\n"          // decrement bitcount
                "brne L_dl0%=\n"             // branch to JP0 if more bits

                // 2 N64 one bits
                // We don't want to loop this twice like we did with the one bits.
                // The N64 controller takes over right after the last one bit a little quicker than expected
                "sbi %[port], 3\n"           // N64 data low
                "nop\nnop\nnop\nnop\nnop\n"  // for 1us
                "nop\nnop\nnop\nnop\nnop\n"
                "cbi %[port], 3\n"           // N64 data high
                "nop\nnop\nnop\nnop\nnop\n"  // for 3us
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"

                "sbi %[port], 3\n"           // N64 data low
                "nop\nnop\nnop\nnop\nnop\n"  // for 1us
                "nop\nnop\nnop\nnop\nnop\n"
                "cbi %[port], 3\n"           // N64 data high
                "nop\nnop\nnop\nnop\nnop\n"  // for ~1.5us
                "nop\nnop\nnop\nnop\nnop\n"  // N64 controller takes over RIGHT after this,
                "nop\nnop\nnop\nnop\nnop\n"  // so we can't wait for 3us without cutting into
                "nop\nnop\nnop\nnop\nnop\n"  // its response. So, we wait for shorter before
                "nop\nnop\nnop\nnop\nnop\n"  // moving on

                : [bitcount] "+r" (bitcount)
                : [port] "I" (_SFR_IO_ADDR(DDRB)) );


  // Listen for the expected 4 bytes of data back from the controller
  // -----------------------------------------------------------------
  for (char b = 0; b < 4; b++)
  {
    asm volatile ("ldi %[bitcount], 8\n"       // bitcount = 8
                  "L_dl0%=:" "\n"              // JP0
                  "ldi %[timeout], 63\n"       // timeout = 63

                  "L_dl1%=:" "\n"              // JP1
                  "sbis %[port], 3\n"          // skip if N64_QUERY
                  "rjmp L_dl2%=\n"             // jump to JP2
                  "dec %[timeout]\n"           // decrement timeout
                  "breq L_dl4%=\n"             // if timeout = 0, jump to JP4
                  "rjmp L_dl1%=\n"             // jump to JP1

                  "L_dl2%=:" "\n"              // JP2
                  "lsl %[data]\n"              // left shift N64_data
                  "nop\nnop\nnop\nnop\nnop\n"  // wait approx 2ms
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\n"
                  "sbic %[port], 3\n"          // query for bit, skip if !N64_QUERY
                  "sbr %[data], 1\n"           // set bit in N64_data
                  "dec %[bitcount]\n"          // decrement bit count
                  "breq L_dl4%=\n"             // if bitcount = 0, jump to JP4

                  "L_dl3%=:" "\n"              // JP3
                  "sbic %[port], 3\n"          // skip if !N64_QUERY
                  "rjmp L_dl0%=\n"             // jump to JP0
                  "dec %[timeout]\n"           // decrement timeout
                  "breq L_dl4%=\n"             // if timeout = 0, jump to JP4
                  "rjmp L_dl3%=\n"             // jump to JP3

                  "L_dl4%=:" "\n"              // JP4 - END
                  "nop\nnop\nnop\nnop\nnop\n"  // wait a little bit before we start the next byte
                  "nop\nnop\nnop\nnop\nnop\n"  // not really sure why this is necessary?
                  "nop\nnop\nnop\nnop\nnop\n"  // if not here, every 8th bit's timing is jittery

                  : [data] "+r" (N64_data[b]), [bitcount] "+r" (bitcount), [timeout] "+r" (timeout)
                  : [port] "I" (_SFR_IO_ADDR(PINB)) );
  }
}

void pack_classic_data(byte *wiimote_registers)
{
  // N64 output in N64_data[]
  //   Byte 0: A, B, Z, Start, Dup, Ddown, Dleft, Dright
  //   Byte 1: 0, 0, L, R, Cup, Cdown, Cleft, Cright
  //   Byte 2: Joystick X value (8 bits)
  //   Byte 3: Joystick Y value (8 bits)

  byte bdl = (N64_data[0] & 0x02 ? 1:0); // D-Pad Left state
  byte bdr = (N64_data[0] & 0x01 ? 1:0); // D-Pad Right state
  byte bdu = (N64_data[0] & 0x08 ? 1:0); // D-Pad Up state
  byte bdd = (N64_data[0] & 0x04 ? 1:0); // D-Pad Down state
  byte ba  = (N64_data[0] & 0x80 ? 1:0); // A button state
  byte bb  = (N64_data[0] & 0x40 ? 1:0); // B button state
  byte bl  = (N64_data[1] & 0x20 ? 1:0); // L button state
  byte br  = (N64_data[1] & 0x10 ? 1:0); // R button state
  byte bs  = (N64_data[0] & 0x10 ? 1:0); // Start button state
  byte bz  = (N64_data[0] & 0x20 ? 1:0); // Z button state
  byte bcu = (N64_data[1] & 0x08 ? 1:0); // C-up button state
  byte bcd = (N64_data[1] & 0x04 ? 1:0); // C-down button state
  byte bcl = (N64_data[1] & 0x02 ? 1:0); // C-left button state
  byte bcr = (N64_data[1] & 0x01 ? 1:0); // C-right button state

  byte bx = 0;
  byte by = 0;

  // N64 returns 8-bit 2's compliment (-127-127 value)
  // Wiimote lx and ly are only 6 bits (0-63 value)
  lx = ((N64_data[2] + 127) >> 2) & 0x3F;
  ly = ((N64_data[3] + 127) >> 2) & 0x3F;

  // If all C-buttons pressed, send X and Y (Wii64 menu)
  if (bcu && bcd && bcl && bcr) { bx = 1; by = 1; }
  // Else C-buttons function like normal
  else
  {
    // C-up and C-down buttons
    if (bcu) ry = 28;
    else if (bcd) ry = 3;
    else ry = 16; //calbuf[11] >> 3; // RY center cal value

    // C-left and C-right buttons
    if (bcl) rx = 3;
    else if (bcr) rx = 28;
    else rx = 16; //calbuf[8] >> 3; // RX center cal value
  }

  // RX<4:3>, LX<5:0>
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  // RX<2:1>, LY<5:0>
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  // RX<0>, LT<4:3>, RY<4:0>
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  // LT<2:0>, RT<4:0>
  wiimote_registers[3] = 0x00;

  // BDR, BDD, BLT, B-, BH, B+, BRT, 1
  wiimote_registers[4] = (bdr << 7) |
                         (bdd << 6) |
                         (bz  << 5) |
                      // (bm  << 4) |   B- not used
                      // (bh  << 3) |   BH not used
                         (bs  << 2) |
                         (br  << 1);

  // BZL, BB, BY, BA, BX, BZR, BDL, BDU
  wiimote_registers[5] = (bl  << 7) |
                         (bb  << 6) |
                         (by  << 5) |
                         (ba  << 4) |
                         (bx  << 3) |
                         (bz  << 2) |
                         (bdl << 1) |
                         bdu;

  // Invert registers 4 and 5 since all buttons are active-low
  wiimote_registers[4] = ~wiimote_registers[4];
  wiimote_registers[5] = ~wiimote_registers[5];
}
