// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//                            +-\/-+
//                  RES/PB5  1|    |8  Vcc
// (yellow) Shift Clk - PB3  2|    |7  PB2/SCLK - Wiimote SCL (blue)
//   (red) Shift Data - PB4  3|    |6  PB1/MISO - Shift Latch (orange)
//                      GND  4|    |5  PB0/MOSI - Wiimote SDA (green)
//                            +----+
// ------------------------------------------------------------------

unsigned int NG_status;

// Neo Geo controller pin defines
#define NG_LATCH_DDR	DDRB
#define NG_LATCH_PORT	PORTB
#define NG_LATCH_BIT	0x02

#define NG_CLOCK_DDR	DDRB
#define NG_CLOCK_PORT	PORTB
#define NG_CLOCK_BIT	0x08

#define NG_DATA_PORT	PORTB
#define NG_DATA_DDR   DDRB
#define NG_DATA_PIN   PINB
#define NG_DATA_BIT   0x10

void controller_init()
{
  // Set Neo Geo controller pin directions
  // Clock and latch as output
  // Data as input
  NG_LATCH_DDR |= NG_LATCH_BIT;
  NG_CLOCK_DDR |= NG_CLOCK_BIT;
  NG_DATA_DDR &= ~(NG_DATA_BIT);

  // Enable pullup on data.
  // This should prevent random toggling
  NG_DATA_PORT |= NG_DATA_BIT;

  // Clock is normally low
  NG_CLOCK_PORT &= ~NG_CLOCK_BIT;

  // Latch is active-low
  NG_LATCH_PORT |= NG_LATCH_BIT;
}

void controller_query()
{
  /*
      Below is how it's wired, but the bytes come out in reverse order.
      I could fix this, but meh... it works.

      Clock Cycle     Button Reported
      ===========     ===============
      1               A
      2               B
      3               C
      4               D
      5               Left
      6               Up
      7               Right
      8               Down
      9               Start
      10              Select
      11              not used (always low)
      12              not used (always low)
      13              not used (always low)
      14              not used (always low)
      15              not used (always low)
      16              not used (always low)
  */
  NG_status = 0;

  // Pulse Latch to store controller state
  NG_LATCH_PORT &= ~NG_LATCH_BIT;

  asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\n");

  NG_LATCH_PORT |= NG_LATCH_BIT;

  asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\n");

  // Shift out the bits
  for (int i = 0; i < 16; i++)
  {
    // Read shifted bit (buttons are active-low)
    NG_status >>= 1;
    if (!(NG_DATA_PIN & NG_DATA_BIT)) NG_status |= 0x8000;

    // Clock the next bit out
    NG_CLOCK_PORT |= NG_CLOCK_BIT;

    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Drop the clock to prepare for next bit
    NG_CLOCK_PORT &= ~NG_CLOCK_BIT;

    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");
  }
}

void pack_classic_data(byte *wiimote_registers)
{
  byte bdl     = (NG_status & 0x08  ? 1:0);  // Joystick Left state
  byte bdr     = (NG_status & 0x02  ? 1:0);  // Joystick Right state
  byte bdu     = (NG_status & 0x04  ? 1:0);  // Joystick Up state
  byte bdd     = (NG_status & 0x01  ? 1:0);  // Joystick Down state
  byte ba      = (NG_status & 0x80  ? 1:0);  // A button state
  byte bb      = (NG_status & 0x40  ? 1:0);  // B button state
  byte bc      = (NG_status & 0x20  ? 1:0);  // C button state
  byte bd      = (NG_status & 0x10  ? 1:0);  // D button state
  byte bstart  = (NG_status & 0x100 ? 1:0); // Start button state
  byte bselect = (NG_status & 0x200 ? 1:0); // Select button state

  if (bdu) ly = 60; //44
  else if (bdd) ly = 3; //20
  else ly = calbuf[5]>>2;

  if (bdr) lx = 60; //44
  else if (bdl) lx = 3; //20
  else lx = calbuf[2]>>2;

  // RX<4:3>, LX<5:0>
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  // RX<2:1>, LY<5:0>
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  // RX<0>, LT<4:3>, RY<4:0>
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  // LT<2:0>, RT<4:0>
  wiimote_registers[3] = 0x00;

  // BDR, BDD, BLT, B-, BH, B+, BRT, 1
  wiimote_registers[4] =
                      // (bdr      << 7) |
                      // (bdd      << 6) |
                      // (bl       << 5) |
                         (bselect  << 4) |
                      // (bh       << 3) |   BH not used
                         (bstart   << 2);
                      // (br       << 1);

  // BZL, BB, BY, BA, BX, BZR, BDL, BDU
  wiimote_registers[5] =
                      // (bzl << 7) |   BZL not used
                         (ba  << 6) |
                         (bc  << 5) |
                         (bb  << 4) |
                         (bd  << 3); //|
                      // (bzr << 2) |   BZR not used
                      // (bdl << 1) |
                      // bdu;

  // Invert registers 4 and 5 since all buttons are active-low
  wiimote_registers[4] = ~wiimote_registers[4];
  wiimote_registers[5] = ~wiimote_registers[5];
}
