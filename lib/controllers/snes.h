// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//                            +-\/-+
//                  RES/PB5  1|    |8  Vcc
//  (yellow) SNES Clk - PB3  2|    |7  PB2/SCLK - Wiimote SCL (blue)
//    (red) SNES Data - PB4  3|    |6  PB1/MISO - SNES Latch (orange)
//                      GND  4|    |5  PB0/MOSI - Wiimote SDA (green)
//                            +----+
// ------------------------------------------------------------------

// SNES controller modes
enum {
  DPAD_ANALOG_MODE,  // Maps d-pad to d-pad AND analog. Good for Wii
  DPAD_MODE,         // Maps d-pad to only d-pad. Wii U doesn't like analog too!
  NES_MODE,          // Remaps Y and B to A and B buttons for NES games
  UNUSED_MODE        // Not sure what to use this for yet?
};
byte SNES_mode = DPAD_ANALOG_MODE;
byte SNES_last_mode;

unsigned int SNES_status;

// SNES controller pin defines
#define SNES_LATCH_DDR	DDRB
#define SNES_LATCH_PORT	PORTB
#define SNES_LATCH_BIT	0x02

#define SNES_CLOCK_DDR	DDRB
#define SNES_CLOCK_PORT	PORTB
#define SNES_CLOCK_BIT	0x08

#define SNES_DATA_PORT	PORTB
#define SNES_DATA_DDR	DDRB
#define SNES_DATA_PIN	PINB
#define SNES_DATA_BIT	0x10

void controller_init()
{
  // Set SNES controller pin directions
  // Clock and latch as output
  // Data as input
  SNES_LATCH_DDR |= SNES_LATCH_BIT;
  SNES_CLOCK_DDR |= SNES_CLOCK_BIT;
  SNES_DATA_DDR &= ~(SNES_DATA_BIT);

  // Enable pullup on data.
  // This should prevent random toggling
  SNES_DATA_PORT |= SNES_DATA_BIT;

  // Clock is normally high
  SNES_CLOCK_PORT |= SNES_CLOCK_BIT;

  // Latch is active-high
  SNES_LATCH_PORT &= ~(SNES_LATCH_BIT);

  // Set last used controller mode
  SNES_mode = EEPROM.read(0);
  if (SNES_mode > 2) SNES_mode = DPAD_MODE;
  SNES_last_mode = SNES_mode;
}

void controller_query()
{
  /*
      Clock Cycle     Button Reported
      ===========     ===============
      1               B
      2               Y
      3               Select
      4               Start
      5               Up on D-pad
      6               Down on D-pad
      7               Left on D-pad
      8               Right on D-pad
      9               A
      10              X
      11              L
      12              R
      13              not used (always high)
      14              not used (always high)
      15              not used (always high)
      16              not used (always high)
  */
  SNES_status = 0;

  // Pulse Latch to store controller state
  SNES_LATCH_PORT |= SNES_LATCH_BIT;

  // Delay ~6us
  asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\n");

  SNES_LATCH_PORT &= ~(SNES_LATCH_BIT);

  // Shift out controller state bits (16 bits in total)
  for (int i = 0; i < 16; i++)
  {
    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Clock the next bit out
    SNES_CLOCK_PORT &= ~(SNES_CLOCK_BIT);

    SNES_status >>= 1;

    // Read shifted bit (buttons are active-low)
    if (!(SNES_DATA_PIN & SNES_DATA_BIT)) SNES_status |= 0x8000;

    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Drop the clock to prepare for next bit
    SNES_CLOCK_PORT |= SNES_CLOCK_BIT;
  }
}

void pack_classic_data(byte *wiimote_registers)
{
  byte bdl     = (SNES_status & 0x40  ? 1:0); // D-Pad Left state
  byte bdr     = (SNES_status & 0x80  ? 1:0); // D-Pad Right state
  byte bdu     = (SNES_status & 0x10  ? 1:0); // D-Pad Up state
  byte bdd     = (SNES_status & 0x20  ? 1:0); // D-Pad Down state
  byte ba      = (SNES_status & 0x100 ? 1:0); // A button state
  byte bb      = (SNES_status & 0x01  ? 1:0); // B button state
  byte bx      = (SNES_status & 0x200 ? 1:0); // X button state
  byte by      = (SNES_status & 0x02  ? 1:0); // Y button state
  byte bl      = (SNES_status & 0x400 ? 1:0); // L button state
  byte br      = (SNES_status & 0x800 ? 1:0); // R button state
  byte bstart  = (SNES_status & 0x08  ? 1:0); // Start button state
  byte bselect = (SNES_status & 0x04  ? 1:0); // Select button state

  // Select + A|B|X to switch between controller modes
  // Selects mode then writes it to EEPROM so we don't forget it when we lose power
  if (bselect && ba)
  {
    SNES_mode = DPAD_ANALOG_MODE;
    bselect = 0;
    ba = 0;
  }
  else if (bselect && bb)
  {
    SNES_mode = DPAD_MODE;
    bselect = 0;
    bb = 0;
  }
  else if (bselect && bx)
  {
    SNES_mode = NES_MODE;
    bselect = 0;
    bx = 0;
  }

  // We only really care to write the new controller mode in EEPROM if it's changed
  // This will stop unnecessary repeated writes to the EEPROM
  if (SNES_mode != SNES_last_mode)
  {
    EEPROM.write(0, SNES_mode);
    SNES_last_mode = SNES_mode;
  }

  // Map D-pad to analog stick for Wii menu control
  // Wii U doesn't like this! That's why we have DPAD_MODE
  if (SNES_mode == DPAD_ANALOG_MODE)
  {
    if (bdu) ly = 44;
    else if (bdd) ly = 20;
    else ly = calbuf[5]>>2;

    if (bdr) lx = 44;
    else if (bdl) lx = 20;
    else lx = calbuf[2]>>2;
  }
  else
  {
    ly = calbuf[5]>>2;
    lx = calbuf[2]>>2;
  }

  // RX<4:3>, LX<5:0>
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  // RX<2:1>, LY<5:0>
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  // RX<0>, LT<4:3>, RY<4:0>
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  // LT<2:0>, RT<4:0>
  wiimote_registers[3] = 0x00;

  // BDR, BDD, BLT, B-, BH, B+, BRT, 1
  wiimote_registers[4] = (bdr      << 7) |
                         (bdd      << 6) |
                         (bl       << 5) |
                         (bselect  << 4) |
                      // (bh       << 3) |   BH not used
                         (bstart   << 2) |
                         (br       << 1);

  // BZL, BB, BY, BA, BX, BZR, BDL, BDU
  wiimote_registers[5] =
                      // (bzl << 7) |   BZL not used
                         ((SNES_mode == NES_MODE ? by : bb)  << 6) |
                         (by  << 5) |
                         ((SNES_mode == NES_MODE ? bb : ba)  << 4) |
                         (bx  << 3) |
                      // (bzr << 2) |   BZR not used
                         (bdl << 1) |
                         bdu;

  // Invert registers 4 and 5 since all buttons are active-low
  wiimote_registers[4] = ~wiimote_registers[4];
  wiimote_registers[5] = ~wiimote_registers[5];
}
