// -----------------------------------------------------------------
// ATtiny85 Chip Pinout
// -----------------------------------------------------------------
//                               +-\/-+
//                     RES/PB5  1|    |8  Vcc
//  (yellow) GENESIS Clk - PB3  2|    |7  PB2/SCLK - Wiimote SCL (blue)
//    (red) GENESIS Data - PB4  3|    |6  PB1/MISO - GENESIS Latch (orange)
//                         GND  4|    |5  PB0/MOSI - Wiimote SDA (green)
//                               +----+
// ------------------------------------------------------------------

byte GENESIS_status;

// Genesis controller pin defines
#define GENESIS_LATCH_DDR   DDRB
#define GENESIS_LATCH_PORT  PORTB
#define GENESIS_LATCH_BIT   0x02

#define GENESIS_CLOCK_DDR   DDRB
#define GENESIS_CLOCK_PORT  PORTB
#define GENESIS_CLOCK_BIT   0x08

#define GENESIS_DATA_PORT   PORTB
#define GENESIS_DATA_DDR    DDRB
#define GENESIS_DATA_PIN    PINB
#define GENESIS_DATA_BIT    0x10

void controller_init()
{
  // Set Genesis controller pin directions
  // Clock and latch as output
  // Data as input
  GENESIS_LATCH_DDR |= GENESIS_LATCH_BIT;
  GENESIS_CLOCK_DDR |= GENESIS_CLOCK_BIT;
  GENESIS_DATA_DDR &= ~(GENESIS_DATA_BIT);

  // Enable pullup on data.
  // This should prevent random toggling
  GENESIS_DATA_PORT |= GENESIS_DATA_BIT;

  // Clock is normally low
  GENESIS_CLOCK_PORT &= ~(GENESIS_CLOCK_BIT);

  // Latch is active-low
  GENESIS_LATCH_PORT |= GENESIS_LATCH_BIT;
}

void controller_query()
{
  /*
      Clock Cycle     Button Reported
      ===========     ===============
      1               Right on D-pad
      2               Down on D-pad
      3               Up on D-pad
      4               Left on D-pad
      5               Start
      6               C
      7               B
      8               A
  */

  GENESIS_status = 0;

  // Pulse Latch to store controller state
  GENESIS_LATCH_PORT &= ~(GENESIS_LATCH_BIT);

  // Delay ~6us
  asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\nnop\n"
                "nop\nnop\nnop\nnop\n");

  GENESIS_LATCH_PORT |= GENESIS_LATCH_BIT;

  // Shift out controller state bits (8 bits in total)
  for (int i = 0; i < 8; i++)
  {
    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Clock the next bit out
    GENESIS_CLOCK_PORT |= GENESIS_CLOCK_BIT;

    GENESIS_status >>= 1;

    // Read shifted bit (buttons are active-low)
    if (!(GENESIS_DATA_PIN & GENESIS_DATA_BIT)) GENESIS_status |= 0x80;

    // Delay ~6us
    asm volatile ("nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\nnop\n"
                  "nop\nnop\nnop\nnop\n");

    // Drop the clock to prepare for next bit
    GENESIS_CLOCK_PORT &= ~(GENESIS_CLOCK_BIT);
  }
}

void pack_classic_data(byte *wiimote_registers)
{
  byte bdl     = (GENESIS_status & 0x08 ? 1:0); // D-Pad Left state
  byte bdr     = (GENESIS_status & 0x01 ? 1:0); // D-Pad Right state
  byte bdu     = (GENESIS_status & 0x04 ? 1:0); // D-Pad Up state
  byte bdd     = (GENESIS_status & 0x02 ? 1:0); // D-Pad Down state
  byte ba      = (GENESIS_status & 0x80 ? 1:0); // A button state
  byte bb      = (GENESIS_status & 0x40 ? 1:0); // B button state
  byte bc      = (GENESIS_status & 0x20 ? 1:0); // C button state
  byte bstart  = (GENESIS_status & 0x10 ? 1:0); // Start button state

  // RX<4:3>, LX<5:0>
  wiimote_registers[0] = ((rx & 0x18) << 3) | (lx & 0x3F);
  // RX<2:1>, LY<5:0>
  wiimote_registers[1] = ((rx & 0x06) << 5) | (ly & 0x3F);
  // RX<0>, LT<4:3>, RY<4:0>
  wiimote_registers[2] = ((rx & 0x01) << 7) | (ry & 0x1F);
  // LT<2:0>, RT<4:0>
  wiimote_registers[3] = 0x00;

  // BDR, BDD, BLT, B-, BH, B+, BRT, 1
  wiimote_registers[4] = (bdr      << 7) |
                         (bdd      << 6) |
                      // (bl       << 5) |
                      // (bselect  << 4) |
                      // (bh       << 3) |   BH not used
                         (bstart   << 2);
                      // (br       << 1);

  // BZL, BB, BY, BA, BX, BZR, BDL, BDU
  wiimote_registers[5] =
                      // (bzl << 7) |   BZL not used
                         (bb  << 6) |
                         (ba  << 5) |
                         (bc  << 4) |
                      // (bx  << 3) |
                      // (bzr << 2) |   BZR not used
                         (bdl << 1) |
                         bdu;

  // Invert registers 4 and 5 since all buttons are active-low
  wiimote_registers[4] = ~wiimote_registers[4];
  wiimote_registers[5] = ~wiimote_registers[5];
}
